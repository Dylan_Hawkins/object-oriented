import uvicorn
from fastapi import FastAPI
from classes import Student, Classroom

students = []
classrooms = []
app = FastAPI()

if __name__ == "__main__":
    uvicorn.run("app:app", reload=True)

def makeName(abr, lvl):
    return abr + "-" + str(lvl)



@app.get("/PLEASE_READ")
async def root():
    return {"message": "Student graders are the best! Have a good day!"}

@app.get("/EXECUTE FOR MINIGAME")
async def game():
    return {"DO ME": "Go to: https://repl.it/@HenryMarshall/BoxChooserWorld#main.py"}

@app.get("/reset")
async def reset():
    #deletes all students and classrooms, resets the sys
    while len(students) > 0:
        del students[0]
    while len(classrooms) > 0:
        del classrooms[0]
    return "Success!"

@app.get("/outbreak")
async def outbreak(day,month,year, infection_type, identifier):
    #date and time of an infection so it can be traced
    date = [day,month,year]
    if infection_type.lower() == "classroom":
        i = -1
        for classroom in classrooms:
            i += 1
            if classroom.classname == identifier:
                break
        else:
            return "class does not exist."
        for student in classrooms[i].students:
            if not student.isVaccinated():
                student.infectionDate = str(date)
        return classrooms[i].students
    else:
        i = -1
        for student in students:
            i += 1
            if student.studentID == identifier:
                break
        else:
            return "student does not exist."
        if not students[i].isVaccinated():
            students[i].infectionDate = str(date)
        return students[i]

@app.get("/save")
async def save():
    with open('data.txt', 'w') as my_data_file:
        my_data_file.write(str(students))
        my_data_file.write("\n")
        my_data_file.write(str(classrooms))
    return "Success!"

@app.get("/student/create")
async def create_student(studentID=0, firstName="", lastName="", Email="", phone="", infectionDate=[-1, -1, -1]):
    #creates a student and adds it to the list of students if it is unique
    s = Student(studentID, firstName, lastName, Email, phone, infectionDate)
    output = [[],[],[]]
    for student in students:
        output[0].append(student.firstName + student.lastName)
        output[1].append(student.email)
        output[2].append(student.studentID)
    if s.studentID not in output[2] and (s.firstName + s.lastName) not in output[0] and s.email not in output[1]:
        students.append(s)
        return s
    else:
        return "User already exists!"

@app.get("/student/read")
async def read_student(studentID):
    #reads the information based on student input
    for student in students:
        if student.studentID == studentID:
            s = student
            break
    else:
        return "Student does not exist!"
    return s

@app.get("/student/update")
async def update_student(studentID=0, firstName="", lastName="", Email="", phone="", infectionDate=[-1, -1, -1]):
    #for changing information of a singular student
    # reads the information based on student input
    i = -1
    for student in students:
        i += 1
        if student.studentID == studentID:
            s = student
            break
    else:
        return "Student does not exist!"
    if firstName != "":
        students[i].firstName = firstName
    if lastName != "":
        students[i].lastName = lastName
    if Email != "":
        students[i].email = Email
    if phone != "":
        students[i].phone = phone
    if infectionDate != "[-1,-1,-1]":
        students[i].infectionDate = infectionDate
    return s

@app.get("/students")
async def all_students():
    return students

@app.get("/student/delete")
async def delete_student(studentID):
    #delete student from list of dictionaries
    i = -1
    for student in students:
        i += 1
        if studentID == student.studentID:
            f = students[i]
            students.remove(student)
            return f
    else:
        return "Student does not exist!"


@app.get("/classroom/create")
async def create_classroom(abr: str, level: int, description: str, days_of_week: str, start_time = [-1,-1,-1], end_time = [-1,-1,-1], professor :str = "", students =[]):
    #add a new object to the class classroom, used for contact tracing
    c = Classroom(abr, level, description, days_of_week, start_time, end_time, professor, students)
    for classroom in classrooms:
        if classroom.classname == c.classname:
            return "Class Already Exists."
    classrooms.append(c)
    return c

@app.get("/classroom/read")
async def read_classroom(abr,lvl):
    #return information of a single classroom
    for classroom in classrooms:
        if makeName(abr, lvl) == classroom.classname:
            return classroom
    return "That class does not currently exist!"

@app.get("/classroom/update")
async def update_classroom(abr: str, level: int, description: str, days_of_week: str, start_time = [-1,-1,-1], end_time = [-1,-1,-1], professor :str = "", students=0):
    #changing information of a singular classroom
    i = -1
    for classroom in classrooms:
        i += 1
        if classroom.classname == makeName(abr,level):
            break
    else:
        return "Classroom not found."
    classrooms[i].description = description
    classrooms[i].days_of_week = days_of_week
    if start_time != "[-1,-1,-1]":
        classrooms[i].start_time = start_time
    if end_time != "[-1,-1,-1]":
        classrooms[i].end_time = end_time
    if professor != "":
        classrooms[i].professor = professor
    if students != "0":
        classrooms[i].students = students
    return classrooms[i]

@app.get("/classroom/students")
async def students_classroom(abr, lvl):
    #list the students in a class room
    for classroom in classrooms:
        if makeName(abr,lvl) == classroom.classname:
            return classroom.students
    return "That class does not currently exist!"

@app.get("/classroom/attendance")
async def attendance_classroom(abr,lvl,day,month,year):
    #Keeping track of the attendance in every classroom, used to understand exposure
    date = [int(day),int(month),int(year)]
    i = -1
    for classroom in classrooms:
        i += 1
        if classroom.classname == makeName(abr,lvl):
            break
    else:
        return "Class does not exist!"
    output = {"present":0,"online":0,"sick":0}
    for student in classrooms[i].students:
        stat = student.status(date)
        if stat == "well":
            output["present"] += 1
        elif stat == "Quar":
            output["online"] += 1
        else:
            output["sick"] += 1
    return output

@app.get("/classroom/enroll_student")
async def enroll_student_classroom(abr,lvl,studentID):
    #Add student from list of dictionaries
    i = -1
    for classroom in classrooms:
        i +=1
        if makeName(abr,lvl) == classroom.classname:
            break
    else:
        return "That class does not exist!"
    j = -1
    for student in students:
        j += 1
        if studentID == student.studentID:
            break
    else:
        return "Student not found!"
    if students[j] in classrooms[i].students:
        return "Student already enrolled"
    else:
        classrooms[i].students.append(students[j])
        return classrooms[i].students

@app.get("/classroom/remove_student")
async def remove_student_classroom(abr,lvl,studentID):
    #delete student from list of dictionaries
    i = -1
    for classroom in classrooms:
        i += 1
        if makeName(abr, lvl) == classroom.classname:
            break
    else:
        return "That class does not exist!"
    j = -1
    for student in students:
        j += 1
        if studentID == student.studentID:
            break
    else:
        return "Student not found!"
    if students[j] in classrooms[i].students:
        classrooms[i].students.remove(students[j])
        return student
    else:
        return "Student not in class!"

@app.get("/classroom/delete")
async def delete_classroom(abr,lvl):
    for classroom in classrooms:
        if makeName(abr, lvl) == classroom.classname:
            f = classroom
            classrooms.remove(classroom)
            return f
    return "That class does not currently exist!"