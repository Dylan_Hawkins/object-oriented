class Classroom:
    def __init__(self, abr: str, level: int, description: str, days_of_week: str, start_time :list = [-1,-1,-1], end_time :list = [-1,-1,-1], professor :str = "", students :list=[]):
        self.abr = abr
        self.level = level
        self.description = description
        self.days_of_week = days_of_week
        self.start_time = start_time
        self.end_time = end_time
        self.professor = professor
        self.students = students
        self.classname = abr + "-" + str(level)

class Student:
    def __init__(self, studentID :int=0, firstName :str="", lastName :str="", Email :str="", phone :str="", infectionDate=[-1, -1, -1]):
        self.studentID = studentID
        self.firstName = firstName
        self.lastName = lastName
        self.email = Email
        self.phone = phone
        self.infectionDate = infectionDate

    def isVaccinated(self):
        self.infectionDate = removeStr2(self.infectionDate, "'")
        r = self.infectionDate.strip('][').split(', ')
        i = -1
        for r1 in r:
            i += 1
            r[i] = int(r[i])
        return r[0] < -1

    def status(self, date):
        self.infectionDate = removeStr2(self.infectionDate,"'")
        r = self.infectionDate.strip('][').split(', ')
        i = -1
        for r1 in r:
            i += 1
            r[i] = int(r[i])
        if r[0] < 0:
            return "well"
        time = (days2(date[2]) - days2(date[2])) + (days(date[1],date[2]) - days(r[1],r[1])) + (date[0] - r[0])
        if time > 14:
            return "well"
        if time > 3:
            return "Quar"
        return "sick"

    def get_phone(self):
        phoney = ""
        for digit in self.phone:
                if len(phoney) < 11 and isDigit(digit):
                    phoney += digit
        return phoney


def isDigit(thing):
    if str(thing) in "0123456789":
        return 1
    return 0

def days(month,year):
    month -= 1
    data = [31,28,31,30,31,30,31,31,30,31,30,31]
    if (year % 4 == 0) and ((year % 100 != 0) == (year % 400 == 0)):
        data[1] += 1
    total = 0
    while month > 0:
        month -= 1
        total += data[month]
    return total

def days2(year):
    total = 0
    while year > 0:
        total += days(12,year)
        year -= 1
    return total

def removeStr2(string, char):
    output = ""
    for letter in string:
        if letter != char:
            output += letter
    return output