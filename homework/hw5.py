class Employee:
    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.fullname = firstname + ' ' + lastname
        self.email = firstname.lower() + '.' + lastname.lower() + '@company.com'


emp_1 = Employee("John", "Smith")
emp_2 = Employee("Mary", "Sue")
emp_3 = Employee("Antony", "Walker")


class Player:
    def __init__(self, name: str, age: int, height: int, weight: int):
        self.name = name
        self.age = age
        self.height = height
        self.weight = weight

    def get_age(self):
        return "{} is age {}".format(self.name, self.age)

    def get_height(self):
        return "{} is {}cm".format(self.name, self.height)

    def get_weight(self):
        return "{} weighs {}kg".format(self.name, self.weight)


p1 = Player("David Jones", 25, 175, 75)


class Name:

    def __init__(self, f_name: str, l_name: str):
        self.f_name = f_name.capitalize()
        self.l_name = l_name.capitalize()
        self.fullname = f_name.capitalize() + ' ' + l_name.capitalize()
        self.initials = f_name[0].upper() + '.' + l_name[0].upper()


a1 = Name("john", "SMITH")


if __name__ == "__main__":
    print(emp_1.fullname)
    print(emp_2.email)
    print(emp_3.firstname)
    print(p1.get_age())
    print(p1.get_height())
    print(p1.get_weight())
    print(a1.f_name)
    print(a1.l_name)
    print(a1.fullname)
    print(a1.initials)

