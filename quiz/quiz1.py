def first(list:[], str):
    ans = ""
    if str.lower() == 'end':
        ans = ('The last element of the list is:', list[-1])
        return ans
    elif str.lower() == 'start' or str.lower() == 'first':
        ans = ('The first element of the list is:', list[0])
        return ans
    else:
        ans = "None"
        return ans


if __name__ == '__main__':
    a = [1, 2, 3, 4, 5, 6, 7]
    string = input("Write end for the last element of the list or start/first for the first element:")
    print(first(a, string))
